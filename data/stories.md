## happy path 01
* greet
  - action_validate_start
  - slot{"isvalidstart" : "sayhi"}
  - utter_main_menu

## happy path 02
* greet
	- action_validate_start
    - slot{"isvalidstart" : "validmoviecode"}
    - utter_movie_info
